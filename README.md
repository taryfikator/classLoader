#W folderze resources załączam ilustrację przeprowadzonych eksperymentów.

# Opis:
W metodzie main jest funkcjonalność wysyłania plików z folderu src/main/resources/public na serwer http://localhost:4567/<b>nazwa pliku</b>.
Za każdym razem gdy plik zostanie uaktualniony w src/main/resources/public, zostanie również uaktualniony na serwerze.

Eksperyment:
1. Początkowa wersja klasy ładowanej przez mój własny classLoader to v0. (ilustracje 1 oraz 2).

2. Nadpisuję plik na serwerze ServiceImpl.class (ilustracje 3 i 4) poprzez małą zmianę na pliku w src/main/resources.

3. Bezpośrednio po nadpisaniu ładowana klasa zmienia swoją wersję (ilustracja 5 oraz 6).

