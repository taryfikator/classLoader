package tokarczyk.magdalena.spark;

import static spark.Spark.get;
import static spark.Spark.staticFiles;

public class Main {

    public static void main(String[] args) throws Exception {

    	String projectDir = System.getProperty("user.dir");
    	String staticDir = "/src/main/resources/public";
    	staticFiles.externalLocation(projectDir + staticDir);


        get("hello/:arg", (req, res) -> {
            return "Hello, " + req.params(":arg") + "!";
        });

        SomeService s1, s2;
        s1 = ServiceFactory.newInstance();

        while (true) {
            s2 = ServiceFactory.newInstance().sync(s1);
            System.out.println("1. " + s1.message() + " counter: " + s1.incr());
            System.out.println("2. " + s2.message() + " counter: " + s2.incr());

            Thread.sleep(2000);
        }

    }

}
