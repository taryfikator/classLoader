package tokarczyk.magdalena.spark;

public class ServiceImpl implements SomeService {

    private int counter;

    @Override
    public String message() {
        return "V 0";
    }

    @Override
    public int incr() {
        return counter++;
    }

    @Override
    public int counter() {
        return counter;
    }

    @Override
    public SomeService sync(SomeService src) {
        counter = src.counter();
        return this;
    }


}
